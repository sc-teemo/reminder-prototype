package layout;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.geometry.Insets;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import csp.*;
import problem.*;

import javax.xml.soap.Text;
import java.io.File;
import java.io.FileInputStream;

import java.util.List;
import java.util.ArrayList;

public class Main extends Application {
    int WINDOW_WIDTH = 750;
    int WINDOW_HEIGHT = 450;
    int FORM_WIDTH = 125;
    int FORM_HEIGHT = 100;
    TextField[] nameFields;
    TextArea[] prefFields;
    List<Object[]> domains;
    String[] variables;
    int amount;
    Group root;

    @Override
    public void start(Stage primaryStage) throws Exception {
        root = new Group();
        Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT, Color.CRIMSON);
        primaryStage.setTitle("Amount of person");

        // Block for POPUP
        Label label1 = new Label("Enter amount of person(s)");

        TextField inputField = new TextField();

        EventHandler<ActionEvent> eventReadInput = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                amount = Integer.parseInt(inputField.getText());
                // error handling for var > 10
                if (amount > 10) {
                    createPopup("Error" ,"Max Variable is 10");
                } else {
                    primaryStage.setScene(scene);
                    primaryStage.setTitle("RE-SYST : Reminder System");
                    createMainScene(amount);
                }
            }
        };

        System.out.println(amount);
        Button readInput = new Button("Next");

        readInput.setOnAction(eventReadInput);

        VBox layout = new VBox(10);

        layout.getChildren().addAll(label1, inputField);

        layout.getChildren().add(readInput);

        System.out.println("Test print");

        layout.setAlignment(Pos.CENTER);

        Scene popupScene = new Scene(layout, 300, 250);

        // Still block for POPUP

        primaryStage.setScene(popupScene);

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

    private GridPane createRectangle(GridPane gridpane, int amountOfCard) {
        this.nameFields = new TextField[amountOfCard];
        this.prefFields = new TextArea[amountOfCard];
        for (int i = 0; i < amountOfCard; i++) {
            Rectangle square = new Rectangle((WINDOW_WIDTH / 5), (WINDOW_HEIGHT / 2) - 20);
            square.setStroke(Color.TRANSPARENT);
            square.setStrokeWidth(1);
            square.fillProperty().set(Color.ANTIQUEWHITE);

            GridPane smallgrid = new GridPane();
            smallgrid.setPadding(new Insets(10, 10, 10, 10));
            Label nameLabel = new Label("Name:");
            smallgrid.add(nameLabel, 0, 1);

            TextField nameTextField = new TextField();
            nameTextField.setPrefWidth(FORM_WIDTH);
            smallgrid.add(nameTextField, 0, 2);

            Label prefLabel = new Label("Preference:");
            smallgrid.add(prefLabel, 0, 3);

            TextArea prefField = new TextArea();
            prefField.setPrefWidth(FORM_WIDTH);
            prefField.setPrefHeight(FORM_HEIGHT);
            smallgrid.add(prefField, 0, 4);

            nameFields[i] = nameTextField;
            prefFields[i] = prefField;

            // System.out.println("This point on square creator");
            if (i >= 5) {
                gridpane.add(square, i - 5, 1);
                gridpane.add(smallgrid, i - 5, 1);
            } else {
                gridpane.add(square, i, 0);
                gridpane.add(smallgrid, i, 0);
            }
        }
        System.out.println("Was on square creator");
        return gridpane;
    }

    public void createMainScene(int amount) {
        GridPane gridpane = new GridPane();
        // gridpane.setPadding(new Insets(10,10,10,10));

        // gridpane.setGridLinesVisible(true);
        Button processAllButton = new Button("Get Assigment");
        EventHandler<ActionEvent> processAll = createCSPProcessor();

        processAllButton.setOnAction(processAll);
        gridpane.add(processAllButton, 4, 2);
        System.out.println("Was on main creator");
        root.getChildren().add(createRectangle(gridpane, amount));
    }

    public EventHandler<ActionEvent> createCSPProcessor() {
        return new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e) {
                domains = new ArrayList<>();
                variables = new String[nameFields.length];
                for (int index = 0; index < nameFields.length; index++) {
                    String name = nameFields[index].getText();
                    String pref = prefFields[index].getText();
                    if (!name.isEmpty()) {
                        System.out.println("Name :" + name);
                        System.out.println("Preference : " + pref);
                        variables[index] = name;
                    }
                }
                String varCheck = checkVar(variables);
                if (!varCheck.equals("")) {
                    createPopup("Error", varCheck);
                } else {    
                    for (int index = 0; index < prefFields.length; index++) {
                        domains.add(prefFields[index].getText().split("\n"));
                    }
                    // Check errors for domains
                    String domCheck = checkDomain(variables, domains);
                    if (!domCheck.equals("")) {
                        createPopup("Error", domCheck);   
                    } else {
                        CSP csp = new ReminderCSP(variables, domains);
                        Assignment results = new Backtracking().solve(csp);
                        if (results == null) {
                            createPopup("Assignment Error","No assigment found. Please re-assign the preference.");
                        } else {
                            createPopup("Assigment Result",results.toString());
                        }    
                    }
                }
            }
        };
    }

    public void createPopup(String title, String message) {
        Stage popupwindow = new Stage();
        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle(title);
        Label label1 = new Label(message.replace("=", " -> ").replace(", ", "\n").replace("{", "").replace("}", ""));

        Button button1 = new Button("OK");
        button1.setOnAction(e -> popupwindow.close());

        VBox layout = new VBox(10);

        layout.getChildren().addAll(label1, button1);

        layout.setAlignment(Pos.CENTER);
        
        int height = title.equalsIgnoreCase("Assigment Result") ? 600 : 100;
        int width = title.equalsIgnoreCase("Assigment Error") ? 600 : 500;
        Scene scene1 = new Scene(layout, width, height);

        popupwindow.setScene(scene1);

        popupwindow.showAndWait();

    }
    /**
     * Error handler for Variables
     * @param variables 
     * @return error message
     */
    public String checkVar(String[] variables) {
        String temp = variables[0];
        for (int i = 0; i < variables.length; i++) {
            if (temp == null) {
                return "Variables cannot be empty";
            }
            if (i + 1 < variables.length && temp.equalsIgnoreCase(variables[i + 1])) {
                return "Variable cannot contain duplicates";
            }
            temp = (i + 1) < variables.length ? variables[i + 1] : variables[i]; 
        }
        return "";
    }

    /**
     * Error handler for Domain
     * @param variables
     * @param domains
     * @return error message
     */
    public String checkDomain(String[] variables, List<Object[]> domains) {
        List<String> varList = new ArrayList<>();
        for (int i = 0; i < variables.length; i++) {
            varList.add(variables[i]);
        }
        int varIndex = 0;
        for (Object[] var : domains) {
            
            String respectiveVar = variables[varIndex];
            Object temp = var[0];
            for (int i = 0; i < var.length; i++) {
                if (((String) var[i]).equals("")) {
                    return "Domains cannot be empty";
                }
                if (!varList.contains(var[i])) {
                    return "There are inconsistencies within the domain";
                }
                if (i + 1 < var.length && ((String) temp).equalsIgnoreCase((String) var[i + 1])) {
                    return "A domain can't contain any duplicates";
                }
                if (((String) var[i]).equalsIgnoreCase(respectiveVar)) {
                    return "A domain can't contain its respective variables";
                }
                temp = (i + 1 < var.length) ? var[i + 1] : var[i];
            }
            varIndex++;
        }
        return "";
    }

}
