package layout;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;

public class Popup {
    public static int AMOUNT;

    public static Stage display(){
        Stage popupwindow=new Stage();

        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle("Set amount of person");

        Label label1= new Label("Enter amount of person(s)");

        TextField inputField = new TextField();

        EventHandler<ActionEvent> eventReadInput = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent e)
            {
                AMOUNT = Integer.parseInt(inputField.getText());
                popupwindow.close();
            }
        };

        Button readInput = new Button("This amount is ok");

        readInput.setOnAction(eventReadInput);

        VBox layout= new VBox(10);

        layout.getChildren().addAll(label1, inputField);

        layout.getChildren().add(readInput);

        System.out.println("Test print");

        layout.setAlignment(Pos.CENTER);

        Scene scene1= new Scene(layout, 200, 100);

        popupwindow.setScene(scene1);

//        popupwindow.showAndWait();

        return popupwindow;
//        return scene1;
    }

    private static void closeWindow(Stage window){

    }

}



