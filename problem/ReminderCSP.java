package problem;

import csp.*;

import java.util.ArrayList;
import java.util.List;

public class ReminderCSP extends CSP {
	public static final int NUM = 4;
	public static final String Andi = "Andi";
	public static final String Budi = "Budi";
	public static final String Caca = "Caca";
	public static final String Doni = "Doni";

	public static final Variable A = new Variable(Andi);
	public static final Variable B = new Variable(Budi);
	public static final Variable C = new Variable(Caca);
	public static final Variable D = new Variable(Doni);

	/**
	 * Returns the reminder candidates from user input
	 * 
	 * @return list of reminders
	 */
	private static List<Variable> collectVariables(String[] candidate) {
		List<Variable> variables = new ArrayList<>();
		for (int i = 0; i < candidate.length ; i++) {
			variables.add(new Variable(candidate[i]));
		}
		return variables;
	}
	/**
	 * Returns the reminder candidates.
	 * 
	 * @return list of reminders
	 */
	private static List<Variable> collectVariables() {
		List<Variable> variables = new ArrayList<Variable>();
		variables.add(A);
		variables.add(B);
		variables.add(C);
		variables.add(D);
		return variables;
	}

	/**
	 * Returns the remindee candidates from user input
	 *
	 * @return list of remindee
	 */
	private static List<Domain> collectDomains(List<Object[]> domains) {
		List<Domain> domainList = new ArrayList<Domain>();
		for (Object[] domain : domains) {
			domainList.add(new Domain(domain));
		}
		return domainList;
	}

	/**
	 * Returns the remindee candidates.
	 * 
	 * @return list of remindee
	 */
	private static List<Domain> collectDomains() {
		List<Domain> domains = new ArrayList<Domain>();
		domains.add(new Domain(new Object[] { Caca, Doni }));
		domains.add(new Domain(new Object[] { Andi, Doni }));
		domains.add(new Domain(new Object[] { Doni }));
		domains.add(new Domain(new Object[] { Andi, Budi, Caca }));
		return domains;
	}

	public void addUniqueValueConstraint(Variable var) {
		for (Variable otherVar : getVariables()) {
			if (!var.equals(otherVar)) {
				addConstraint(new NotEqualConstraint(var, otherVar));
			}
		}
	}
	
	/**
	 * Constructs a reminder system.
	 */
	public ReminderCSP(String[] variables, List<Object[]> domains) {
		// super(collectVariables());
		super(collectVariables(variables));
		List<Variable> reminders = getVariables();
		// List<Domain> remindees = collectDomains();
		List<Domain> remindees = collectDomains(domains);

		for (int i = 0; i < reminders.size(); i++) {
			setDomain(reminders.get(i), remindees.get(i));
		}

		for (Variable reminder : reminders) {
			addUniqueValueConstraint(reminder);
		}
	}

}