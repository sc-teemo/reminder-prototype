package csp;

/**
 * A Backtracking algorithm to solve CSP.
 * It uses the AC3 algorithm to reduce variables domains first.
 */
public class Backtracking {

	public Assignment solve(CSP csp) {
		// Reduce Domain first using AC3
		DomainRestore info = new AC3().reduceDomains(csp);
		if (!info.isEmpty()) {
			if (info.isEmptyDomainFound())
				return null;
		}
		return recursiveBackTrackingSearch(csp, new Assignment());
	}

	private Assignment recursiveBackTrackingSearch(CSP csp,
			Assignment assignment) {
		Assignment result = null;
		// Base Case
		if (assignment.isComplete(csp.getVariables())) {
			result = assignment;
		} else {
			// Set assignment for a variable
			Variable var = selectUnassignedVariable(assignment, csp);
			for (Object value : orderDomainValues(var, assignment, csp)) {
				assignment.setAssignment(var, value);
				
				if (assignment.isConsistent(csp.getConstraints(var))) {
					DomainRestore info = inference(var, assignment, csp);
					if (!info.isEmptyDomainFound()) {
						result = recursiveBackTrackingSearch(csp, assignment);
						if (result != null)
							break;
					}
					info.restoreDomains(csp);
				}
				assignment.removeAssignment(var);
			}
		}
		return result;
	}

	/**
	 * 
	 * @return the first unassigned variable found
	 */
	protected Variable selectUnassignedVariable(Assignment assignment, CSP csp) {
		for (Variable var : csp.getVariables()) {
			if (!(assignment.hasAssignmentFor(var)))
				return var;
		}
		return null;
	}

	/**
	 * 
	 * @return list of domain values in original order
	 */
	protected Iterable<?> orderDomainValues(Variable var,
			Assignment assignment, CSP csp) {
		return csp.getDomain(var);
	}

	/**
	 * Do an inference for a variable
	 * @return assignment candidate for a variable
	 */
	protected DomainRestore inference(Variable var, Assignment assignment,
			CSP csp) {
			return new AC3().reduceDomains(var,
					assignment.getAssignment(var), csp, assignment);
	}

}
