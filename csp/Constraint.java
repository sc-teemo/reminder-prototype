package csp;

import java.util.List;

/**
 * An interface for any concrete Constraint
 */
public interface Constraint {
	List<Variable> getScope();

	boolean isSatisfiedWith(Assignment assignment);
}