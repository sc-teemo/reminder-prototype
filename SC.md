CSP:
CSP(List vars)
- list variable
- list domain
- list constraint
- var, int
- var, list constraints (constraint network)

Variable
<!-- A variable is a distinguishable object with a name. -->
- name

Domain - Iterable 
<!-- Domain Di consists of a set of allowable values for variable Vi-->
- list values

Constraint
<!-- allowable combinations -->
<!-- 
 * A constraint specifies the allowable combinations of values for a set of
 * variables. Each constraint consists of a pair <scope, rel>, where scope is a
 * tuple of variables that participate in the constraint and rel is a relation
 * that defines the values that those variables can take on 
 -->
 <!-- Returns a tuple of variables that participate in the constraint. -->
List<Variable> getScope();

<!-- Constrains the values that the variables can take on. -->
boolean isSatisfiedWith(Assignment assignment);

Assignment (interface)
<!-- An assignment assigns values to some or all variables of a CSP. -->
- assigned variables
- var, value
- isConsistent
- isComplete
- isSolution


DomainRestoreInfo
