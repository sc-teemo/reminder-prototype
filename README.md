# RE-SYST : Reminder System
A project dedicated for SC group Teemo using CSP.

This app can find a reminder system assignment based on preferences of each person. This reminder system assign a person to remind another person. Every person must remind one person and also reminded by one person. In this case, a person is part of variable (as reminder) and also part of domain (as remindee).

# How to run

## GUI version

type in terminal:
`javac layout/Main.java`
`java layout/Main`

Insert the name of all person that join this reminder system and their preference (the name of people that he/she prefer to remind). The output assignment will be popped up. If there's no possible assignment, this app will give you error message.

## CLI version

type in terminal:
`javac problem/CSPSimulator.java`
`java layout/CSPSimulator`

Follow the instruction and the app will be show the assignment.

## Sample Problem

type in terminal:
`javac problem/ReminderCSP.java`
`java layout/ReminderCSP`

This class has built in problem (domain and variable) and it will automatically show the assignment and the execution time.

# Members
- Bimo Iman Smartadi (1706039780)
- Dafa Ramadanysah (1706039370)
- Reyhan Alhafizal (1706040082)
